import 'package:get/get.dart';
import 'package:kia_mobile/views/home_page.dart';
import 'package:kia_mobile/views/login/login_screen.dart';

abstract class AppRoutes {
  AppRoutes._();

  static final routes = <GetPage>[
    GetPage(name: "/home", page: () => HomePage()),
    GetPage(name: "/login", page: () => LoginScreen())
  ];
}
