import 'package:flutter/material.dart';

import 'package:kia_mobile/views/login/login_page.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoginPage(),
    );
  }
}
