import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kia_mobile/controllers/auth_controller.dart';
import 'package:kia_mobile/controllers/ibuhamil_controller.dart';
import 'package:kia_mobile/views/login/background.dart';

class HomePage extends StatefulWidget {
  HomePage({Key? key}) : super(key: key);

  @override
  _HomeListPageState createState() => _HomeListPageState();
}

class _HomeListPageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    var token = Get.find<AuthController>().authModel.value.accessToken;
    print("cek");
    IbuhamilController ibuhamilController = Get.find<IbuhamilController>();
    return Scaffold(
      appBar: AppBar(
        leading: const IconButton(
          icon: Icon(Icons.logout),
          onPressed: null,
        ),
        backgroundColor: Colors.green,
        title: const Text('List Pasien'),
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              height: 170,
              width: double.infinity,
              decoration: BoxDecoration(color: Colors.green),
              padding: new EdgeInsets.all(7),
              child: new Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    'Nama bidan',
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
      // body: Stack(
      //   children: [
      //     Container(
      //       width: double.infinity,
      //       height: 170,
      //       padding: new EdgeInsets.only(left: 0.0, bottom: 8.0, right: 16.0),
      //       decoration: new BoxDecoration(color: Colors.green),
      //       child: Column(
      //         children: [Text('nama')],
      //       ),
      //     ),
      //     Container(
      //       padding: EdgeInsets.only(top: 100),
      //       height: 100,
      //       width: 200,
      //       margin: EdgeInsets.all(20),
      //       child: Card(
      //         color: Colors.blue,
      //       ),
      //     )
      //   ],
      // ),
    );
  }
}
