import 'dart:convert';

import 'package:equatable/equatable.dart';

class AuthModel extends Equatable {
  final String accessToken;
  final String tokenType;
  AuthModel({
    required this.accessToken,
    required this.tokenType,
  });

  AuthModel copyWith({
    String? accessToken,
    String? tokenType,
  }) {
    return AuthModel(
      accessToken: accessToken ?? this.accessToken,
      tokenType: tokenType ?? this.tokenType,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'accessToken': accessToken,
      'tokenType': tokenType,
    };
  }

  factory AuthModel.fromMap(Map<String, dynamic> map) {
    return AuthModel(
      accessToken: map['accessToken'],
      tokenType: map['tokenType'],
    );
  }

  String toJson() => json.encode(toMap());

  factory AuthModel.fromJson(String source) =>
      AuthModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props => [accessToken, tokenType];
}
