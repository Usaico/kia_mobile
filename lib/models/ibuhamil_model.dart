import 'dart:convert';

import 'package:equatable/equatable.dart';

class Ibuhamil extends Equatable {
  final int idBumil;
  final String name;
  final String umur;
  final String pendidikan;
  final String alamat;
  final String kec;
  final String kab;
  final String pekerjaan;
  final String hamilKe;
  final String heidTerakhir;
  final String persalinan;
  Ibuhamil({
    required this.idBumil,
    required this.name,
    required this.umur,
    required this.pendidikan,
    required this.alamat,
    required this.kec,
    required this.kab,
    required this.pekerjaan,
    required this.hamilKe,
    required this.heidTerakhir,
    required this.persalinan,
  });

  Ibuhamil copyWith({
    int? idBumil,
    String? name,
    String? umur,
    String? pendidikan,
    String? alamat,
    String? kec,
    String? kab,
    String? pekerjaan,
    String? hamilKe,
    String? heidTerakhir,
    String? persalinan,
  }) {
    return Ibuhamil(
      idBumil: idBumil ?? this.idBumil,
      name: name ?? this.name,
      umur: umur ?? this.umur,
      pendidikan: pendidikan ?? this.pendidikan,
      alamat: alamat ?? this.alamat,
      kec: kec ?? this.kec,
      kab: kab ?? this.kab,
      pekerjaan: pekerjaan ?? this.pekerjaan,
      hamilKe: hamilKe ?? this.hamilKe,
      heidTerakhir: heidTerakhir ?? this.heidTerakhir,
      persalinan: persalinan ?? this.persalinan,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id_bumil': idBumil,
      'name': name,
      'umur': umur,
      'pendidikan': pendidikan,
      'alamat': alamat,
      'kec': kec,
      'kab': kab,
      'pekerjaan': pekerjaan,
      'hamil_ke': hamilKe,
      'heid_terakhir': heidTerakhir,
      'persalinan': persalinan
    };
  }

  factory Ibuhamil.fromMap(Map<String, dynamic> map) {
    return Ibuhamil(
      idBumil: map['id_bumil'],
      name: map['name'],
      umur: map['umur'],
      pendidikan: map['pendidikan'],
      alamat: map['alamat'],
      kec: map['kec'],
      kab: map['kab'],
      pekerjaan: map['pekerjaan'],
      hamilKe: map['hamil_ke'],
      heidTerakhir: (map['heid_terakhir']),
      persalinan: (map['persalinan']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Ibuhamil.fromJson(String source) =>
      Ibuhamil.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      idBumil,
      name,
      umur,
      pendidikan,
      alamat,
      kec,
      kab,
      pekerjaan,
      hamilKe,
      heidTerakhir,
      persalinan,
    ];
  }
}
