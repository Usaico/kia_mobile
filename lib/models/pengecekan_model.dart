import 'dart:convert';

import 'package:equatable/equatable.dart';

class PengecekanModel extends Equatable {
  final int idPengecekan;
  final int jmlSkor;
  final String kategori;
  final String perawatan;
  final String rujukan;
  final String tempat;
  final String penolong;
  final String rujukanRDB;
  final String rujukanRDR;
  final String rujukanRTW;
  final int periodePengecekan;
  PengecekanModel({
    required this.idPengecekan,
    required this.jmlSkor,
    required this.kategori,
    required this.perawatan,
    required this.rujukan,
    required this.tempat,
    required this.penolong,
    required this.rujukanRDB,
    required this.rujukanRDR,
    required this.rujukanRTW,
    required this.periodePengecekan,
  });

  PengecekanModel copyWith({
    int? idPengecekan,
    int? jmlSkor,
    String? kategori,
    String? perawatan,
    String? rujukan,
    String? tempat,
    String? penolong,
    String? rujukanRDB,
    String? rujukanRDR,
    String? rujukanRTW,
    int? periodePengecekan,
  }) {
    return PengecekanModel(
      idPengecekan: idPengecekan ?? this.idPengecekan,
      jmlSkor: jmlSkor ?? this.jmlSkor,
      kategori: kategori ?? this.kategori,
      perawatan: perawatan ?? this.perawatan,
      rujukan: rujukan ?? this.rujukan,
      tempat: tempat ?? this.tempat,
      penolong: penolong ?? this.penolong,
      rujukanRDB: rujukanRDB ?? this.rujukanRDB,
      rujukanRDR: rujukanRDR ?? this.rujukanRDR,
      rujukanRTW: rujukanRTW ?? this.rujukanRTW,
      periodePengecekan: periodePengecekan ?? this.periodePengecekan,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'id_pengecekan': idPengecekan,
      'jml_skor': jmlSkor,
      'kategori': kategori,
      'perawatan': perawatan,
      'rujukan': rujukan,
      'tempat': tempat,
      'penolong': penolong,
      'rujukan_RDB': rujukanRDB,
      'rujukan_RDR': rujukanRDR,
      'rujukan_RTW': rujukanRTW,
      'periode_pengecekan': periodePengecekan,
    };
  }

  factory PengecekanModel.fromMap(Map<String, dynamic> map) {
    return PengecekanModel(
      idPengecekan: map['id_pengecekan'],
      jmlSkor: map['jml_skor'],
      kategori: map['kategori'],
      perawatan: map['perawatan'],
      rujukan: map['rujukan'],
      tempat: map['tempat'],
      penolong: map['penolong'],
      rujukanRDB: map['rujukan_RDB'],
      rujukanRDR: map['rujukan_RDBrujukan_RDBrujukan_RDB'],
      rujukanRTW: map['rujukan_RTW'],
      periodePengecekan: map['periode_pengecekan'],
    );
  }

  String toJson() => json.encode(toMap());

  factory PengecekanModel.fromJson(String source) =>
      PengecekanModel.fromMap(json.decode(source));

  @override
  bool get stringify => true;

  @override
  List<Object> get props {
    return [
      idPengecekan,
      jmlSkor,
      kategori,
      perawatan,
      rujukan,
      tempat,
      penolong,
      rujukanRDB,
      rujukanRDR,
      rujukanRTW,
      periodePengecekan,
    ];
  }
}
